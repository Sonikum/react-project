import React from 'react'
import './navbar.css'
import { BiSearchAlt2 } from "react-icons/bi";
import { MdOutlineBrightness7 } from "react-icons/md";
import { BsCalendar4 } from "react-icons/bs";
import { GoClock } from "react-icons/go";
import { BsFillGrid3X3GapFill } from "react-icons/bs";
import { BsArrowLeft } from "react-icons/bs";
import { HiOutlineMinus } from "react-icons/hi";
import { BsArrowRight } from "react-icons/bs";
import { BsFacebook } from "react-icons/bs";
import { FaInstagramSquare } from "react-icons/fa";
import { AiFillTwitterCircle } from "react-icons/ai";
import { BsLinkedin } from "react-icons/bs";
import { AiFillAndroid } from "react-icons/ai";
import { BsApple } from "react-icons/bs";




const Navbar = () => {
    return (
        <>
        <nav className="nav11">
            <div className="mainnav">
            <li><a href="#">Free Online Courses</a></li>
            <li><a href="#">Resources</a></li>
            <li><a href="#">Corporate Training</a></li>
            <li><a href="#">Become an Instructor</a></li>
            <li><a href="#">Higher Educator</a></li>
            
            </div>
        </nav>
        <nav className="sec-nav">
        <div className="second-nav">
            <img className="simple" src="simple.jpeg" alt="here is pic"></img>
            <button className="button"><BsFillGrid3X3GapFill /> All Course</button>
            <input type="text" value="What do you want to learn?" />
            <button className="login">Log in</button>
        </div>
        </nav>
         <div className="image11">
         <img className="img1" src="banner_v1.jpg" />
         </div>
         <div className="img-content">
            <div className="contect"> <h1> World’s #1<br/> Online Bootcamp</h1>
            <p><b>2,000,000</b> careers advanced <br/><b>1,500</b> live classes every month <br/><b>85% </b>report career benefits including promotion or a new job</p>
            <button className="btn">Explore Programs</button></div>
         <div className="screen"> <img className="scnimg" src="screen.png"></img></div>
         </div>
         <p className="ftr">Partnering with world's leading universities and companies</p>
         <center><img className="university" src="university-d-v1.png"></img></center>
         <div className="footer1">
         <p className="paragraph1">Get Certified, Get Ahead with Our Programs</p>
         <h2 className="head">Post Graduate Programs</h2>
         <p className="paragraph2">Learn from global experts and get certified by the world's leading universities</p>
         </div>
         <div className="list-img">
         
         <div><li><MdOutlineBrightness7 color="blue"/> University Certificates</li></div>
         <div><li><MdOutlineBrightness7 color="blue"/> University Alumni Status</li></div>
         <div><li><MdOutlineBrightness7 color="blue"/> Masterclasses from University</li></div>
         <div><li><MdOutlineBrightness7 color="blue"/> Masterclasses from University</li></div>
         <div><li><MdOutlineBrightness7 color="blue"/> Career Support</li></div>
         </div>
         <div className="three-image">
         <div className="div1"><img className="first1" src="first.jpg"></img>
         <div className="inner-content">
             <h3>Post Graduating Programs in cloud Computing</h3>
             <li className="date"><BsCalendar4 /> 12 Months</li>
             <li className="date"><GoClock /> Cohort Starts : 18 Nov, 2021 </li>
             <li><img className="download" src="download.png" /></li>
         </div></div>

         <div className="div1"><img className="second2" src="second.jpg"></img> 
         <div className="inner-content">            
          <h3>Post Graduating Programs in data science</h3>
          <li className="date"><BsCalendar4 /> 12 Months</li>
             <li className="date"><GoClock />Cohort Starts : 17 Nov, 2021 </li>
             <li><img className="download" src="download.png" /></li>
          </div></div>
         <div className="div1"><img className="second3" src="second.jpg"></img>
         <div className="inner-content">             
         <h3>Post Graduating Programs in cloud Computing</h3>
         <li className="date"><BsCalendar4 /> 6 Months</li>
         <li className="date"><GoClock /> Cohort Starts : 11 Nov, 2021 </li>
         <li><img className="download" src="download.png" /></li>
         </div>
         </div>
         </div>

         
        <center><div className="arrow"><BsArrowLeft /><HiOutlineMinus /><BsArrowRight /></div></center>
        <div className="cpntent1"> <h2>Master's Programs</h2>
        <p className="archieve">Achieve your career goals with industry-recognized learning paths</p>
        </div>
         
         <footer>
        <div class="footer11">
          
         <div className="social"> <h5>Follow Us!</h5> <button className="Refer">Refer and Earn</button> <br/>
           <div className="social-pic">
           <BsFacebook />
           < FaInstagramSquare />
           <AiFillTwitterCircle />
           <BsLinkedin />
           </div>
           </div>

           <div>
             <h5>About Us</h5>
             <p>careers</p>
             <p>In The Media</p>
             <p>Alumni Speak</p>
             <p>Contact Us</p>
           </div>

           <div>
               <h5>Become an instructor</h5>
               <p>Blog as guest</p>
           </div>

           <div>
           <h5>Skillup</h5>
           <p>Resources</p>
           <p>Simpli learn community</p>
           <p>RSS feed</p>
           <p>Simplilearn Coupons and <br/> Discount offers</p>
           <p>City Sitemap</p>
           </div>

           <div >
              <h5>Corporate Training</h5>
              <p>Partners</p>
              <p>Digital Transformation</p>
           </div>

           <div className="android">
              <button className="App">< AiFillAndroid/> Get the Android App</button><br/>
              <button className="ios"><BsApple />Get the iOS App</button>
           </div>
        </div>
        <hr />

        <div className="traing1">
          <h3> Trending Post Graduate Programs</h3>
          <p>Project Management Certification Course | Cyber Security Certification Course | Data Science Bootcamp Program | Data Analytics Bootcamp Program | Business Analysis Certification Course | Digital Marketing Certification Program | Lean Six Sigma Certification Course | DevOps Certification Course | Cloud Computing Certification Course | Data Engineering Course | AI and Machine Learning Course | Full Stack Web Development Course
          </p>
        </div>

        <div className="traing1">
          <h3> Trending Master Programs</h3>
        <p>PMP Plus Certification Training Course | Big Data Engineer Course | Data Science Certification Course | Data Analyst Certification Course | Artificial Intelligence Course | Cloud Architect Certification Training Course | DevOps Engineer Certification Training Course | Advanced Digital Marketing Course | Cyber Security Expert Course | MEAN Stack Developer Course</p>
        </div>

        <div className="traing1">
          <h3 > Trending Resources</h3>

          <p>PMP Certification Training Course | Big Data Hadoop Certification Training Course | 
             Data Science with Python Certification Course | Machine Learning Certification Course | 
             AWS Solutions Architect Certification Training Course | CISSP Certification Training | 
             Certified ScrumMaster (CSM) Certification Training | ITIL 4 Foundation Certification Training Course | 
             Java Certification Course | Python Certification Training Course</p>
        </div>
        <div>
       </div>
        </footer>
        </>   
    )
}
export default Navbar;
